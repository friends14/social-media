import React, { useState } from "react"
import Grid from "@material-ui/core/Grid"
import { makeStyles } from "@material-ui/core/styles"
import containerBackground from "../../images/logi_register_background.jpg"
import Login from "./Login"
import SignUp from "./SignUp"

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        height: "100%",
        backgroundImage: `url(${containerBackground})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
    },
}))
const AuthContainer = () => {
    const [state, setstate] = useState({ flag: false })
    const classes = useStyles()
    return (
        <Grid
            className={classes.root}
            container
            alignItems="center"
            justify="center"
        >
            {state.flag ? <Login /> : <SignUp />}
        </Grid>
    )
}

export default AuthContainer
