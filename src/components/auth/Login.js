import React from "react"
import logo from "../../images/logo.png"
const Login = () => {
    return (
        <div
            className="signup"
            style={{
                backgroundColor: "rgb(0,0,0,0.8)",
                color: "white",
                padding: "2rem",
            }}
        >
            <img
                src={logo}
                alt=""
                style={{ width: "15rem", marginBottom: "1rem" }}
            />
            <form style={{ display: "flex", flexDirection: "column" }}>
                <input
                    style={inputStyle}
                    type="text"
                    name="username"
                    id="username"
                    placeholder="username"
                />

                <input
                    style={inputStyle}
                    type="password"
                    name="Password"
                    id="Password"
                    placeholder="Password"
                />
                <input
                    type="submit"
                    value="login"
                    style={{
                        marginBottom: "1rem",
                        width: "15rem",
                        height: "2rem",
                        backgroundColor: "rgb(243,173,0)",
                        color: "white",
                        fontSize: "1.04rem",
                        borderRadius: "4px",
                    }}
                />
            </form>
        </div>
    )
}
const inputStyle = {
    marginBottom: "1rem",
    width: "15rem",
    height: "2rem",
    borderRadius: "4px",
    paddingLeft: "0.5rem",
}

export default Login
