import React from 'react';

import { BrowserRouter , Route, Switch } from 'react-router-dom';
import routes from '../routes'
const MainApp = () => {
  return (
    <BrowserRouter>
      <Switch>
       {routes.map(el=>{
         return <Route {...el} />
       })}
      </Switch>
    </BrowserRouter>
  );
};

export default MainApp;
