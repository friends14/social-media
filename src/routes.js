import AuthContainer from "./components/auth/AuthContainer"
import NotFound from "./pages/NotFound"
const routes = [
    {
        path: "/registeration",
        component: AuthContainer,
    },
    {
        component: NotFound,
    },
]
export default routes
